import { useState } from 'react'
import reactLogo from './assets/react.svg'
import viteLogo from '/vite.svg'
import Content from './Components/Content'
import Description from './Components/Description'
import CompanyDetail from './Components/CompanyDetail'
function App() {
  const [count, setCount] = useState(0)

  return (
    <>
      <Description/>
      {/* <CompanyDetail/> */}
        <Content/>
      
    </>
  )
}

export default App
