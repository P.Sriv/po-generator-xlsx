import React, { useState } from "react";
import CompanyDetail from "./CompanyDetail";
import { getBook } from "../Service/Excel_Handler";
import { Company } from "../Service/Excel_Handler";
import { Order } from "./Order";
const Content = () => {
  
  interface Item{ 
    name: string; 
    price: number;
     qty: number; 
     note: string; 
  }
  
   
    const [issuerCompany,setIssuerCompany] = useState<Company>({name:'',tax:'',postal:'',tel:'',address:'',po:'',email:'',date:new Date().toISOString().substr(0, 10),corperation:'จำกัดq',fax:''}) 
    const [suplierCompany,setSuplierCompany] = useState<Company>({name:'',tax:'',postal:'',tel:'',address:'',po:'',email:'',date:new Date().toISOString().substr(0, 10),corperation:'จำกัด',fax:''}) 

    
  
    const handleIssuerCompanyData = (e:React.ChangeEvent<any>)=>{
    
      const {name,value} = e.target
      
      setIssuerCompany( {...issuerCompany,[name]:value})
    }
    const handleSuplierCompanyData = (e:React.ChangeEvent<any>)=>{
      const {name,value} = e.target
      setSuplierCompany( {...suplierCompany,[name]:value})
    }
  

    
  return (
    <>
      <div className="container overflow-x-auto flex flex-col m-4">
        <CompanyDetail company={issuerCompany} sendDetail ={handleIssuerCompanyData} side="Issuer"/>
        <CompanyDetail company={suplierCompany} sendDetail={handleSuplierCompanyData} side="Suplier"/>
        <Order />
          
            
      </div>
      <div className="container flex justify-end gap-2">
       <button className="btn btn-active btn-primary w-1/8 " onClick={(e)=>getBook(issuerCompany,[])}>generate</button> 

      </div>
    </>
  );
};

export default Content;
