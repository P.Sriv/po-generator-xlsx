import { useState } from "react";
export const Order = ()=>{
      
  interface Item{ 
    name: string; 
    price: number;
     qty: number; 
     note: string; 
  }
  
    const [item,setItem] =  useState<Item []>([
        { name: '', price: 0, qty: 0, note: '' }
      ]);
      const setItems = (index:number,event: React.ChangeEvent<HTMLInputElement>)=>{
          const {name,value} = event.target
          const newData = [...item]
          newData[index] = {...newData[index],[name]:value}
          setItem(newData)
      }
      const deleteColumn=(indexToRemove:number)=>{
        if(indexToRemove !=0){
        setItem(
         item.filter((i,index) => index !== indexToRemove)
        )
        }
      }
      const addColumn =(event:React.MouseEvent<HTMLButtonElement>)=>{
        setItem([...item,{name: '', price: 0, qty: 0, note: ''}]);
      }
      
     
    return (
        <>
        <h1 className=" text-3xl font-bold m-2">Item Order</h1>
        <table className="table">
        
          <thead>
            <tr>
              <th>Order</th>
              <th>Name</th>
              <th>Price</th>
              <th>Quantity</th>
              <th>Total</th>
              <th>Note</th>
            </tr>
          </thead>
          <tbody>
           {item.map((i,index)=>(
            
            <tr key={index}>
                <td> {index+1}</td>
                <td><input type="text" value={i.name as string} name="name" onChange={(e)=>setItems(index,e)}></input></td>
                <td><input type="number" value={i.price as number} name="price" onChange={(e)=>setItems(index,e)}></input></td>
                <td><input type="number" value={i.qty as number} name="qty" onChange={(e)=>setItems(index,e)}></input></td>
                <td> {item[index].price * item[index].qty}</td>
                <td><input type="text" value={i.note as string} name="note" onChange={(e)=>setItems(index,e)}></input></td>
                <td>
                <button className=" w-1/8 " onClick={(e)=>deleteColumn(index)}>
                  <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" strokeWidth={1.5} stroke="currentColor" className="w-6 h-6 items-center">
                  <path strokeLinecap="round" strokeLinejoin="round" d="M6 18 18 6M6 6l12 12" />
                  </svg>
              </button>
                </td>
            </tr>
           ))}
          </tbody>
        </table>
        <button className="btn btn-active btn-primary w-1/8 " onClick={(e)=>addColumn(e)}>new</button>

            
      
    
        </>
    )
}