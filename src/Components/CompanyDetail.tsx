import { useState } from "react";
import { Company } from "../Service/Excel_Handler";
const CompanyDetail = ({ sendDetail,side ,company}: { sendDetail: any,side:string,company:Company }) => {


  return (
    <>
      <div className="divider"></div>
      <div className="container flex flex-col m-auto gap-2 ">
        <h1 className=" text-2xl font-bold">{side}</h1>
        <div className="flex gap-2 items-center ">
          <p>Company name:</p>
          <input
            type="text"
            placeholder="Type here"
            className="input input-bordered input-sm w-full max-w-xs"
            name="name"
            value={company.name}
            onChange={e=>sendDetail(e)}
          />
            <select className=" dropdown-content w-28" defaultValue={company.corperation} onChange={e=>sendDetail(e)} name="corperate">
            <option value="จำกัด" > จำกัด</option>
            <option value="จำกัดมหาชน"> จำกัด มหาชน</option>
            </select>
          

          <p className="">เลขผู้เสียภาษี:</p>
          <input
            type="text"
            placeholder="Type here"
            className="input input-bordered input-sm w-full max-w-xs"
            name="tax"
            value={company.tax}
            onChange={e=>sendDetail(e)}
          />
        </div>
        <div className=" flex gap-2 items-center  ">
          <p className="">Postal Code:</p>
          <input
            type="number"
            placeholder="Type here"
            className="input input-bordered input-sm w-full max-w-xs"
            name="postal"
            value={company.postal}
            onChange={e=>sendDetail(e)}
          />

          <p className="">Tel:</p>
          <input
            type="number"
            placeholder="Type here"
            className="input input-bordered input-sm w-full max-w-xs"
            name="tel"
            value={company.tel}
            onChange={e=>sendDetail(e)}
          />
          <p>Fax:</p>
          <input
            type="number"
            placeholder="Type here"
            className="input input-bordered input-sm w-full max-w-xs"
            name="fax"
            value={company.fax}
            onChange={e=>sendDetail(e)}
          />
        </div>
        <div className=" flex gap-2 items-center ">
          <p className="">Address</p>
          <input
            type="text"
            placeholder="Type here"
            className="input input-bordered input-md w-full max-w-xs basis-full"
            name="address"
            value={company.address}
            onChange={e=>sendDetail(e)}
          />
        </div>
        <div className=" flex gap-2 items-center ">
          <p> Purchase Date:</p>
          <input
            type="date"
            placeholder="Type here"
            className="input input-bordered input-sm w-full max-w-xs"
            name="date"
            value={company.date}
            onChange={e=>sendDetail(e)}
          />
          <p>P.O Number</p>
          <input
            type="number"
            placeholder="Type here"
            className="input input-bordered input-sm w-full max-w-xs"
            name="po"
            value={company.po}
            onChange={e=>sendDetail(e)}
          />
        </div>
        <div className=" flex gap-2 items-center ">
          <p className="">Email</p>
          <input
            type="email"
            placeholder="Type here"
            className="input input-bordered input-sm w-full max-w-xs"
            name="email"
            value={company.email}
            onChange={e=>sendDetail(e)}
          />
        </div>
      </div>
      <div className="flex justify-end">
        <button
          className="btn btn-active btn-primary w-1/6 "
          // onClick={() => setDefault(company)}
        >
          Save
        </button>
      </div>

    </>
  );
};

export default CompanyDetail;
