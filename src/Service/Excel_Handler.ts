import Excel, { Worksheet } from  'exceljs'
import { saveAs } from 'file-saver';
export interface Company{
    name:string
    tax: string,
    postal : string,
    tel :string,
    address:string,
    po: string,
    email : string,
    date : string,
    corperation : string,
    fax: string,

  }

export const getBook = async(company:Company,items:any[])=>{

const workbook= new Excel.Workbook()
var worksheet = workbook.addWorksheet('Order')
worksheet = setIssuer(worksheet,company)
const buffer = await workbook.xlsx.writeBuffer()
saveAs(new Blob([buffer]), `PO-${company.po}.xlsx`);



}
const setIssuer=(sheet:Worksheet,company:Company)=>{
  const template = [
    [`บริษัท ${company.name} ${company.corperation}`],
    [`ที่อยู่ ${company.address} ${company.postal}`],
    [`โทร ${company.tel} โทรสาร ${company.fax} อีเมลล์ ${company.email}`],
    [`เลขประจำตัวผู้เสียอากร ${company.tax} (สำนักงานใหญ่)`]
  ]

  const style = {
    family: 2,
    size: 20,
    bold:true,
    italic: true
  }
 
  template.forEach((val,index)=>{
    var newRow = sheet.getCell(index,3)
    newRow.value   = val[0]
    newRow.style.font = style

  })
 
  return sheet
} 